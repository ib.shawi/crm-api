<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200501223442 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE email (id INT AUTO_INCREMENT NOT NULL, record_id INT NOT NULL, email VARCHAR(255) NOT NULL, INDEX IDX_E7927C744DFD750C (record_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE name (id INT AUTO_INCREMENT NOT NULL, record_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_5E237E064DFD750C (record_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE records (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tel (id INT AUTO_INCREMENT NOT NULL, record_id INT NOT NULL, tel INT NOT NULL, INDEX IDX_F037AB0F4DFD750C (record_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE email ADD CONSTRAINT FK_E7927C744DFD750C FOREIGN KEY (record_id) REFERENCES records (id)');
        $this->addSql('ALTER TABLE name ADD CONSTRAINT FK_5E237E064DFD750C FOREIGN KEY (record_id) REFERENCES records (id)');
        $this->addSql('ALTER TABLE tel ADD CONSTRAINT FK_F037AB0F4DFD750C FOREIGN KEY (record_id) REFERENCES records (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE email DROP FOREIGN KEY FK_E7927C744DFD750C');
        $this->addSql('ALTER TABLE name DROP FOREIGN KEY FK_5E237E064DFD750C');
        $this->addSql('ALTER TABLE tel DROP FOREIGN KEY FK_F037AB0F4DFD750C');
        $this->addSql('DROP TABLE email');
        $this->addSql('DROP TABLE name');
        $this->addSql('DROP TABLE records');
        $this->addSql('DROP TABLE tel');
    }
}
