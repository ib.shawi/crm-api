<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EmailRepository")
 */
class Email
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *@Assert\Length(max=255)
     */
    private $email;
  /**
    * @ORM\ManyToOne(targetEntity="App\Entity\Records")
    * @ORM\JoinColumn(nullable=false)
    */
   private $record;




    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }


    public function getRecord()
    {
        return $this->record;
    }


    public function setRecord($record): void
    {
        $this->record = $record;
    }


}
