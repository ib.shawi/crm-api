<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NameRepository")
 */
class Name
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     *@Assert\Length(max=255)
     */
    private $name;
  /**
    * @ORM\ManyToOne(targetEntity="App\Entity\Records")
    * @ORM\JoinColumn(nullable=false)
    */
   private $record;




    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


    public function getRecord()
    {
        return $this->record;
    }


    public function setRecord($record): void
    {
        $this->record = $record;
    }


}
