<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TelRepository")
 */
class Tel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $Tel;
  /**
    * @ORM\ManyToOne(targetEntity="App\Entity\Records")
    * @ORM\JoinColumn(nullable=false)
    */
   private $record;




    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTel()
    {
        return $this->Tel;
    }

    public function setTel(int $tel)
    {
        $this->Tel = $tel;

        return $this;
    }


    public function getRecord()
    {
        return $this->record;
    }


    public function setRecord($record): void
    {
        $this->record = $record;
    }


}
