<?php
namespace App\Controller;

use App\Entity\Records;
use App\Entity\Name;
use App\Entity\Email;
use App\Entity\Tel;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Form\AddType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\JsonResponse;

class homeController extends Controller
{

  /**
   * @Route("/",name="home")
   *@Method({"GET"})
   */
  public function index()
  {
    return new JsonResponse(null,Response::HTTP_NO_CONTENT
    );

  }


  /**
   * @Route("/all",name="all")
   */
  public function all()
  {
    $names=$this->getDoctrine()->getRepository(Name::class)->findBy([],['record'=>'DESC']);
return $this->json(array_map(function(Name $names){return array( 'name'=>$names->getName(),'recordId'=>$names->getRecord()->getId());},$names));


  }


  /**
   * @Route("/show/{id}",name="show")
   */
  public function show($id)
  {


$record=array();
$record['id']=$id;
$record['name'] = $this->getDoctrine()->getRepository(Name::class)->findOneBy(['record' =>$id])->getName();
$record['email'] = $this->getDoctrine()->getRepository(Email::class)->findOneBy(['record' =>$id])->getEmail();
$record['tel'] = $this->getDoctrine()->getRepository(Tel::class)->findOneBy(['record' =>$id])->getTel();

      return new JsonResponse(['record'=>$record]);

  }


    /**
     * @Route("/edit/{id}",name="edit")
     */
    public function edit($id)
    {

      $record=array();
      $record['Id']=$id;
      $record['Name'] = $this->getDoctrine()->getRepository(Name::class)->findOneBy(['record' =>$id])->getName();
      $record['Email'] = $this->getDoctrine()->getRepository(Email::class)->findOneBy(['record' =>$id])->getEmail();
      $record['Tel'] = $this->getDoctrine()->getRepository(Tel::class)->findOneBy(['record' =>$id])->getTel();

            return new JsonResponse(['record'=>$record,'url'=>'edit2']);
  }

  /**
   * @Route("/edit2",name="edit2")
   */
  public function edit2(Request $request,ObjectManager $manager)
  {
    $array=json_decode($request->getContent());

    $email=$this->getDoctrine()->getRepository(Email::class)->findOneBy(['record' =>$array->Id]);
    $name=$this->getDoctrine()->getRepository(Name::class)->findOneBy(['record' =>$array->Id]);
    $tel=$this->getDoctrine()->getRepository(Tel::class)->findOneBy(['record' =>$array->Id]);

    $email->setEmail($array->Email);
    $name->setName($array->Name);
    $tel->setTel($array->Tel);

    $manager->persist($email);
    $manager->persist($name);
    $manager->persist($tel);
    $manager->flush();

    return $this->redirectToRoute('all');

    }


      /**
       * @Route("/delete/{id}",name="delete")
       */
      public function delete(Request $request,$id,ObjectManager $manager)
      {

      $record=$this->getDoctrine()->getRepository(Records::class)->findOneBy(['id' =>$id]);
      $email=$this->getDoctrine()->getRepository(Email::class)->findOneBy(['record' =>$id]);
        $name=$this->getDoctrine()->getRepository(Name::class)->findOneBy(['record' =>$id]);
          $tel=$this->getDoctrine()->getRepository(Tel::class)->findOneBy(['record' =>$id]);

        $manager->remove($record);
        $manager->remove($email);
        $manager->remove($name);
        $manager->remove($tel);
        $manager->flush();

        return $this->redirectToRoute('all');

      }


  /**
   * @Route("/add",name="add")
   */
  public function add(Request $request,ObjectManager $manager)
  {
    $record=new Records();
    $manager->persist($record);
    $manager->flush();

    $email=new Email();
    $name=new Name();
    $tel=new Tel();

    $array=json_decode($request->getContent());
    $email->setEmail($array->Email);
    $name->setName($array->Name);
    $tel->setTel($array->Tel);

    $email->setRecord($record);
    $name->setRecord($record);
    $tel->setRecord($record);

    $manager->persist($email);
    $manager->persist($name);
    $manager->persist($tel);
    $manager->flush();

    return $this->redirectToRoute('all');
}

}
