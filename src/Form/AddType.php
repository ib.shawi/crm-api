<?php

namespace App\Form;

use App\Entity\Records;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AddType extends AbstractType
{
  // private function getConfiguration($label,$placeholder,$options=[]){
  //   return array_merge(['label'=>$label,'attr'=>['placeholder'=>$placeholder]],$options);
  // }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Name',TextType::class,array('attr'=>array('class'=>'form-control')))
            ->add('Email',TextType::class,array('attr'=>array('class'=>'form-control')))
            ->add('Tel',IntegerType::class,array('attr'=>array('class'=>'form-control')))
            //->add('save',SubmitType::class,array('label'=>'Create','attr'=>array('class'=>'btn btn-primary mt3')))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Records::class,
        ]);
    }
}
